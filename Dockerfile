FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt -y update \
  && apt -y upgrade \
  && apt -y install git rpm ruby ruby-dev rubygems build-essential \
  && rm -rf /var/lib/apt/lists/*

RUN gem install --no-document fpm

